//Search
const sButton = document.querySelector('.button')
sButton.addEventListener('click', function() {

    const inputKey = document.querySelector('.input')
    fetch(
      `https://newsapi.org/v2/everything?q=${inputKey.value}&apiKey=11ded9cb17864dc29ba4af514a07be56`
      )
      .then(response => response.json())
      .then(response => presentNews(response))
      .catch((err) => console.log(err))
    })

//Trend
fetch(
    'https://newsapi.org/v2/everything?q=Business&sortBy=popularity&apiKey=11ded9cb17864dc29ba4af514a07be56'
    )
    .then(response => response.json())
    .then(response => presentNews(response))
    .catch((err) => console.log(err))

//fPresent
function presentNews(response) {
    let news = response.articles
    let cards = ''
    news.forEach((news) => (cards) += `

      <div class="news d-inline-flex flex-container m-5">
        <div class="card" style="width:20rem;">
          <img src="${news.urlToImage}" class="card-img-top image-fluid img-news">
          <div class="cards-body">
            <h5 class="cards-title p-2">${news.title}</h5>
            <p class="cards-text p-2">${news.description}</p>
            <div class="d-flex justify-content-center">
              <a href="${news.url}" class="btn btn-outline-warning justify m-5">Continue reading ...</a>
            </div>
          </div>
        </div>
      </div>

        `)
    const newsContainer = document.querySelector('.news-container')
    newsContainer.innerHTML = cards
}





