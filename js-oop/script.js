import { Member, Card } from "./libs.js"


const tableContainer = document.getElementById('table');
const dataTable = new Member({
    columns: ['Nama Depan', 'Nama Belakang', 'Usia', 'Email', 'Asal'],
    data: [
        ['Tasya', 'Nur', 20, 'tasnss@example.com', 'Tangerang'],
        ['Tamara', 'Gusmilanda', 20, 'tamtam@gmail.com', 'Medan'],
        ['Farmeila', 'Sarah', 22, 'sarah123@gmail.com', 'Jambi'],
        ['Siti', 'Ulfiatur', 23, 'ulful@gmail.com', 'Kebumen'],
        ['Harum', 'Budiasih', 20, 'hrmsd@gmail.com', 'Tangerang'],
        ['Fajar', 'Ambar', 20, 'fajrrramd@note.com', 'Yogyakarta']
    ]
})

dataTable.render(tableContainer);

const cardContainer = document.getElementById("card");
// const data = ;
const dataCard = new Card(
    [{
            text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime mollitia,\
    molestiae quas vel sint commodi repudiandae consequuntur voluptatum laborum \
    numquam blanditiis harum quisquam eius sed odit fugiat iusto fuga praesentium \
    optio, eaque rerum!',
            image: 'image/pict1.jpeg',
            name: 'Ir. Soekarno',
            job: 'Presiden ke-1',
        },
        {
            text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime mollitia,\
    molestiae quas vel sint commodi repudiandae consequuntur voluptatum laborum \
    numquam blanditiis harum quisquam eius sed odit fugiat iusto fuga praesentium \
    optio, eaque rerum!',
            image: 'image/pict2.jpeg',
            name: 'Soeharto',
            job: 'Presiden ke-2',
        }, {
            text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime mollitia,\
    molestiae quas vel sint commodi repudiandae consequuntur voluptatum laborum \
    numquam blanditiis harum quisquam eius sed odit fugiat iusto fuga praesentium \
    optio, eaque rerum!',
            image: 'image/pict3.jpeg',
            name: 'B.J. Habibie',
            job: 'Presiden ke-3',
        },
    ]);

dataCard.render(cardContainer);






// let myTable = document.querySelector('#table');


// let employees = [
//     { name: 'Tasya', age: 20, email: 'tasnss@example.com' },
//     { name: 'Tamara', age: 21, email: 'tamtam@gmail.com' },
//     { name: 'Ulfi', age: 22, email: 'ulful@gmail.com' },
//     { name: 'Harum', age: 20, email: 'hrmsd@gmail.com' },
//     { name: 'Sarah', age: 20, email: 'sarah123@gmail.com' }
// ]


// let headers = ['Name', 'Age', 'Email'];


// let table = document.createElement('table');
// let headerRow = document.createElement('tr');

// headers.forEach(headerText => {
//     let header = document.createElement('th');
//     let textNode = document.createTextNode(headerText);
//     header.appendChild(textNode);
//     headerRow.appendChild(header);
// });

// table.appendChild(headerRow);

// employees.forEach(emp => {
//     let row = document.createElement('tr');

//     Object.values(emp).forEach(text => {
//         let cell = document.createElement('td');
//         let textNode = document.createTextNode(text);
//         cell.appendChild(textNode);
//         row.appendChild(cell);
//     })

//     table.appendChild(row);
// });

// myTable.appendChild(table);